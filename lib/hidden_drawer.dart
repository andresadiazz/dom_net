import 'package:dom_n_net/bienestar/screen/home_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hidden_drawer_menu/hidden_drawer_menu.dart';

class HiddenDrawer extends StatefulWidget {
  const HiddenDrawer({Key? key}) : super(key: key);

  @override
  State<HiddenDrawer> createState() => _HiddenDrawerState();
}

class _HiddenDrawerState extends State<HiddenDrawer> {
  List<ScreenHiddenDrawer> _pages = [];

  @override
  void initState() {
    super.initState();
    _pages = [
      ScreenHiddenDrawer(
          ItemHiddenMenu(
              name: 'Bienestar',
              colorLineSelected: const Color(0xffC1DEE2),
              baseStyle: GoogleFonts.roboto(
                  color: const Color(0xffF8FAFD), fontSize: 20),
              selectedStyle:
                  GoogleFonts.roboto(color: const Color(0xffF8FAFD))),
          const BienestarPage())
    ];
  }

  @override
  Widget build(BuildContext context) {
    return HiddenDrawerMenu(
      backgroundColorMenu: Theme.of(context).primaryColor,
      screens: _pages,
      initPositionSelected: 0,
      slidePercent: 60,
    );
  }
}
