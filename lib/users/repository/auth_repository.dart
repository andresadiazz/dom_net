import 'package:equatable/equatable.dart';

class AuthUser extends Equatable {
  final String uid;
  final String email;

  const AuthUser(this.uid, this.email);

  @override
  // TODO: implement props
  List<Object?> get props => [uid, email];
}

abstract class AuthRepository {
  AuthUser? get authUser;

  Stream<AuthUser?> get onAuthStateChanged;

  Future<AuthUser?> signInWithEmailAndPassword(String email, String password);

  Future<AuthUser?> createUserWithEmailAndPassword(
      String email, String password);

  Future<void> signOut();
}
