import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class MyUser extends Equatable {
  final String id;
  final String name;
  final String email;
  final String country;
  final String city;
  final String genero;
  final String facebookFollowers;
  final String instagramFollowers;
  final String youtubeFollowers;
  final String spotifyFollowers;
  final String? image;
  final String? description;
  final String? ingresos;
  final String? spotifyLink;
  final String? instagramLink;
  final String? youtubeLink;
  final String? facebookLink;
  final String? whatsappLink;
  final String? demoLink;
  final String? pressKitLink;
  final String? riderTecnicoLink;
  final String? cotizacionlink;
  final String? nivel;
  final String? nivelNumber;
  final int? spotifyValue;
  final int? ingresosValue;
  final String? spotifyOyentes;
  final String? niveles;
  final Timestamp? lastSignIn;

  const MyUser(
      this.id,
      this.name,
      this.email,
      this.country,
      this.city,
      this.genero,
      this.facebookFollowers,
      this.instagramFollowers,
      this.youtubeFollowers,
      this.spotifyFollowers,
      this.description,
      this.ingresos,
      this.spotifyLink,
      this.instagramLink,
      this.youtubeLink,
      this.facebookLink,
      this.whatsappLink,
      this.demoLink,
      this.pressKitLink,
      this.riderTecnicoLink,
      this.cotizacionlink,
      this.nivel,
      this.nivelNumber,
      this.spotifyValue,
      this.ingresosValue,
      this.spotifyOyentes,
      this.niveles,
      this.lastSignIn,
      this.image);

  Map<String, Object> toMap() {
    return <String, Object>{'uid': id};
  }

  Map<String, Object?> toFirebaseMap({String? newImage}) {
    return <String, Object?>{
      'uid': id,
      'name': name,
      'email': email,
      'country': country,
      'city': city,
      'genre': genero,
      'Facebook': facebookFollowers,
      'instagram': instagramFollowers,
      'youtube': instagramFollowers,
      'spotify': spotifyFollowers,
      'imageProfile': image,
      'description': description,
      'ingresos': ingresos,
      'spotifyLink': spotifyLink,
      'instagramLink': instagramLink,
      'youtubeLink': youtubeLink,
      'facebookLink': facebookLink,
      'whatsappLink': whatsappLink,
      'demoLink': demoLink,
      'pressKitLink': pressKitLink,
      'riderTécnicoLink': riderTecnicoLink,
      'cotizacionlink': cotizacionlink,
      'nivel': nivel,
      'nivelNumber': nivelNumber,
      'spotifyValue': spotifyValue,
      'ingresosValue': ingresosValue,
      'spotifyOyentes': spotifyOyentes,
      'niveles': niveles,
      'lastSignIn': lastSignIn
    };
  }

  MyUser.fromFirebaseMap(Map<String, Object?> data)
      : id = data['uid'] as String,
        name = data['name'] as String,
        email = data['email'] as String,
        country = data['country'] as String,
        city = data['city'] as String,
        genero = data['genre'] as String,
        facebookFollowers = data['Facebook'] as String,
        instagramFollowers = data['instagram'] as String,
        youtubeFollowers = data['youtube'] as String,
        spotifyFollowers = data['spotify'] as String,
        image = data['imageProfile'] as String?,
        description = data['description'] as String,
        ingresos = data['ingresos'] as String,
        spotifyLink = data['spotifyLink'] as String,
        instagramLink = data['instagramLink'] as String,
        youtubeLink = data['youtubeLink'] as String,
        facebookLink = data['facebookLink'] as String,
        whatsappLink = data['whatsappLink'] as String,
        demoLink = data['demoLink'] as String,
        pressKitLink = data['pressKitLink'] as String,
        riderTecnicoLink = data['riderTécnicoLink'] as String,
        cotizacionlink = data['cotizacionlink'] as String,
        nivel = data['nivel'] as String,
        nivelNumber = data['nivelNumber'] as String,
        spotifyValue = data['spotifyValue'] as int,
        ingresosValue = data['ingresosValue'] as int,
        spotifyOyentes = data['spotifyOyentes'] as String,
        niveles = data['niveles'] as String,
        lastSignIn = data['lastSignIn'] as Timestamp;

  @override
  List<Object?> get props => [
        id,
        name,
        email,
        country,
        city,
        genero,
        facebookFollowers,
        youtubeFollowers,
        instagramFollowers,
        spotifyFollowers,
        image,
        description,
        ingresos,
        spotifyLink,
        instagramLink,
        youtubeLink,
        facebookLink,
        whatsappLink,
        demoLink,
        pressKitLink,
        riderTecnicoLink,
        cotizacionlink,
        nivel,
        nivelNumber,
        spotifyValue,
        ingresosValue,
        spotifyOyentes,
        niveles,
        lastSignIn
      ];
}
