import 'dart:async';

import 'package:get/get.dart';

import '../navigation/routes.dart';
import '../repository/auth_repository.dart';

enum AuthState { signedOut, signIn }

class AuthController extends GetxController {
  final _authRepository = Get.find<AuthRepository>();
  late StreamSubscription _authSubscription;

  final Rx<AuthState> authState = Rx(AuthState.signedOut);
  final Rx<AuthUser?> authUser = Rx(null);

  void _authStateChanged(AuthUser? user) {
    if (user == null) {
      authState.value = AuthState.signedOut;
      // TODO: Navegar al intro de la app
      Get.offAllNamed(Routes.intro);
    } else {
      authState.value = AuthState.signIn;
      //TODO: Navegar al home
      Get.offAllNamed(Routes.home);
    }
    authUser.value = user;
  }

  @override
  void onInit() async {
    // TODO: implement onInit
    await Future.delayed(const Duration(seconds: 5));
    _authSubscription =
        _authRepository.onAuthStateChanged.listen(_authStateChanged);
    super.onInit();
  }

  Future<void> signOut() async {
    await _authRepository.signOut();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    _authSubscription.cancel();
    super.onClose();
  }
}
