import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../model/user.dart';
import '../repository/my_user_repository.dart';
import 'auth_controller.dart';

class MyUserController extends GetxController {
  final _userRepository = Get.find<MyUserRepository>();

  final nameController = TextEditingController();
  final lastNameController = TextEditingController();
  final ageController = TextEditingController();

  Rx<File?> pickedImage = Rx(null);
  Rx<bool> isLoading = Rx(false);
  Rx<bool> isSaving = Rx(false);
  Rx<MyUser?> user = Rx(null);

  @override
  void onInit() {
    getMyUser();
    super.onInit();
  }

  void setImage(File? imageFile) async {
    pickedImage.value = imageFile;
  }

  Future<void> getMyUser() async {
    isLoading.value = true;
    user.value = await _userRepository.getMyUser();
    nameController.text = user.value?.name ?? '';

    isLoading.value = false;
  }

  /* Future<void> saveMyUser() async {
    isSaving.value = true;
    final uid = Get.find<AuthController>().authUser.value!.uid;
    final email = Get.find<AuthController>().authUser.value!.email;
    final name = nameController.text;
    final newUser = MyUser(uid, name, email,  image: user.value?.image);
    user.value = newUser;

    // For testing add delay
    await Future.delayed(const Duration(seconds: 3));
    await _userRepository.saveMyUser(newUser, pickedImage.value);
    isSaving.value = false;
  } */

  Future<void> createMyUser() async {
    final uid = Get.find<AuthController>().authUser.value!.uid;
    final email = Get.find<AuthController>().authUser.value!.email;
    final newUser = MyUser(
        uid,
        "",
        email,
        "",
        "",
        "",
        "0",
        "0",
        "0",
        "0",
        "",
        "0",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        0,
        0,
        "0",
        "",
        Timestamp.fromDate(DateTime.now()),
        "");
    await _userRepository.saveMyUser(newUser, pickedImage.value);
  }
}
