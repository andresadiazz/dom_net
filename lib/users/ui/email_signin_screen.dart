import 'package:dom_n_net/users/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import '../controllers/email_signin_controller.dart';

class EmailSignIn extends StatefulWidget {
  const EmailSignIn({Key? key}) : super(key: key);

  @override
  State<EmailSignIn> createState() => _EmailSignInState();
}

class _EmailSignInState extends State<EmailSignIn> {
  final _formKey = GlobalKey<FormState>();
  bool showPassword = false;

  @override
  Widget build(BuildContext context) {
    final emailController = Get.put(EmailSignInController());
    return Container(
      padding: const EdgeInsets.all(24),
      child: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset(
                "assets/images/Wireframe.png",
                width: 150,
                height: 150,
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                'DOM&NET SAS',
                style: GoogleFonts.roboto(fontSize: 32),
              ),
              Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Obx(() => Visibility(
                          visible: emailController.isLoading.value,
                          child: const Center(
                            child: CircularProgressIndicator(),
                          ))),
                      Obx(() => Visibility(
                          visible:
                              emailController.error.value?.isNotEmpty == true,
                          child: Text(emailController.error.value ?? ''))),
                      const SizedBox(
                        height: 8,
                      ),
                      SizedBox(
                        height: 50,
                        child: TextFormField(
                          controller: emailController.emailController,
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 15),
                          decoration: InputDecoration(
                              hintText: "E-mail",
                              filled: true,
                              fillColor: Colors.grey[200],
                              hintStyle: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 15),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey[300]!),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(8),
                              )),
                          keyboardType: TextInputType.emailAddress,
                          validator: emailController.emailValidator,
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        height: 50,
                        child: TextFormField(
                            controller: emailController.passwordController,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 15),
                            decoration: InputDecoration(
                                hintText: "Contraseña",
                                filled: true,
                                fillColor: Colors.grey[200],
                                hintStyle: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 15),
                                suffixIcon: TextButton(
                                    onPressed: () {
                                      setState(() {});
                                      showPassword = !showPassword;
                                    },
                                    child: !showPassword
                                        ? Text(
                                            'Show',
                                            style: GoogleFonts.roboto(
                                                color: Theme.of(context)
                                                    .backgroundColor),
                                          )
                                        : Text(
                                            'Hide',
                                            style: GoogleFonts.roboto(
                                                color: Theme.of(context)
                                                    .backgroundColor),
                                          )),
                                enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey[300]!),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius: BorderRadius.circular(8),
                                )),
                            keyboardType: TextInputType.emailAddress,
                            validator: emailController.emailValidator,
                            obscureText: !showPassword),
                      ),
                      TextButton(
                          onPressed: () {},
                          child: Text(
                            "¿Se te olvidó la contraseña?",
                            style: GoogleFonts.raleway(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.w700,
                                decoration: TextDecoration.underline),
                          )),
                      const SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        width: double.infinity,
                        // ignore: deprecated_member_use
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          onPressed: () {
                            if (_formKey.currentState?.validate() == true) {
                              emailController.signInWithEmailAndPassword();
                            }
                          },
                          padding: const EdgeInsets.symmetric(vertical: 13),
                          color: Theme.of(context).backgroundColor,
                          child: Text(
                            'Log In',
                            style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                                letterSpacing: 0.8),
                          ),
                        ),
                      ),
                    ],
                  )),
              const Divider(),
              TextButton(
                  onPressed: () {
                    Get.toNamed(Routes.createAccount);
                  },
                  child: Text(
                    '¿Quieres crear una cuenta?',
                    style: GoogleFonts.roboto(
                        color: Theme.of(context).backgroundColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
