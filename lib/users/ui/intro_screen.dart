import 'package:dom_n_net/users/ui/email_signin_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_indicator/page_indicator.dart';

import '../controllers/home_signin_controller.dart';
import '../controllers/my_user_controller.dart';

class IntroScreen extends StatelessWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _IntroPage(),
    );
  }
}

class _IntroPage extends HookWidget {
  final String text1 =
      "Puedes controlar todos los sensores que tengas activados en tu casa desde nuestra App.";
  final String text2 =
      "Puedes tener toda la información de cada sensor que tengas instalado en tu casa.";

  final String title1 = "¡Tu tienes el control!";
  final String title2 = "Obtén toda la información a tu mano";

  @override
  Widget build(BuildContext context) {
    final homeSignInController = Get.put(HomeSignInController());
    return AbsorbPointer(
      absorbing: homeSignInController.isLoading.value,
      child: PageIndicatorContainer(
        child: PageView(
          children: [
            _DescriptionPage(
                text: text1,
                title: title1,
                imagePath: "assets/images/Home.png"),
            _DescriptionPage(
                text: text2,
                title: title2,
                imagePath: "assets/images/Wireframe.png"),
            _LoginPage()
          ],
          controller: usePageController(),
        ),
        align: IndicatorAlign.bottom,
        length: 3,
        indicatorSpace: 12,
        indicatorColor: Colors.white30,
        indicatorSelectorColor: Colors.white,
        shape: IndicatorShape.oval(),
      ),
    );
  }
}

class _LoginPage extends StatefulWidget {
  @override
  State<_LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<_LoginPage> {
  final userController = Get.put(MyUserController());
  final bool _aceptacion = false;

  @override
  Widget build(BuildContext context) {
    final homeSignInController = Get.find<HomeSignInController>();

    return Container(
        color: Theme.of(context).shadowColor,
        child: const Center(child: EmailSignIn()));
  }
}

class _LoginButton extends StatelessWidget {
  final String text;
  final IconData icon;
  final VoidCallback? onTap;
  final Color? color;
  final Color? textColor;
  final Color? colorBorder;

  const _LoginButton(
      {Key? key,
      required this.text,
      required this.icon,
      this.onTap,
      this.color,
      this.textColor,
      this.colorBorder})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      // ignore: deprecated_member_use
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
          side: BorderSide(color: colorBorder!),
        ),
        onPressed: onTap,
        padding: const EdgeInsets.symmetric(vertical: 13),
        color: color,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: textColor,
              size: 20,
            ),
            const SizedBox(
              width: 5,
            ),
            Text(
              text,
              style: GoogleFonts.roboto(
                  color: textColor,
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  letterSpacing: 0.8),
            ),
          ],
        ),
      ),
    );
  }
}

class _DescriptionPage extends StatelessWidget {
  final String text;
  final String imagePath;
  final String title;

  const _DescriptionPage(
      {Key? key,
      required this.text,
      required this.imagePath,
      required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(color: Theme.of(context).backgroundColor),
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 30, bottom: 20),
              child: Container(
                height: media.height * 0.5,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                        image: AssetImage(imagePath), fit: BoxFit.contain)),
              ),
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: GoogleFonts.roboto(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 30,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              text,
              textAlign: TextAlign.center,
              style: GoogleFonts.roboto(
                color: Colors.black,
                fontWeight: FontWeight.w400,
                fontSize: 16,
              ),
            )
          ],
        ),
      ),
    );
  }
}
