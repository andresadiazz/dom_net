import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import '../controllers/email_create_controller.dart';
import '../controllers/my_user_controller.dart';

class EmailCreate extends StatefulWidget {
  const EmailCreate({Key? key}) : super(key: key);

  @override
  State<EmailCreate> createState() => _EmailCreateState();
}

class _EmailCreateState extends State<EmailCreate> {
  final _formKey = GlobalKey<FormState>();

  final userController = Get.find<MyUserController>();

  bool showPassword = false;

  @override
  Widget build(BuildContext context) {
    final emailController = Get.put(EmailCreateController());
    return Scaffold(
      body: Center(
        child: Form(
            key: _formKey,
            child: Container(
              padding: const EdgeInsets.all(24),
              child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/Wireframe.png",
                        width: 150,
                        height: 150,
                      ),
                      Text(
                        'Dom&Net',
                        style: GoogleFonts.roboto(fontSize: 32),
                      ),
                      Text(
                        'Sing Up',
                        style: GoogleFonts.roboto(fontSize: 20),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Obx(() => Visibility(
                          visible: emailController.isLoading.value,
                          child: const Center(
                            child: CircularProgressIndicator(),
                          ))),
                      Obx(() => Visibility(
                          visible:
                              emailController.error.value?.isNotEmpty == true,
                          child: Text(emailController.error.value ?? ''))),
                      const SizedBox(
                        height: 8,
                      ),
                      SizedBox(
                        height: 50,
                        child: TextFormField(
                          controller: emailController.emailController,
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 15),
                          decoration: InputDecoration(
                              hintText: "E-mail",
                              filled: true,
                              fillColor: Colors.grey[200],
                              hintStyle: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 15),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey[300]!),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              border: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                              )),
                          keyboardType: TextInputType.emailAddress,
                          validator: emailController.emailValidator,
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        height: 50,
                        child: TextFormField(
                            controller: emailController.passwordController,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 15),
                            decoration: InputDecoration(
                                hintText: "Contraseña",
                                filled: true,
                                fillColor: Colors.grey[200],
                                suffixIcon: TextButton(
                                    onPressed: () {
                                      setState(() {});
                                      showPassword = !showPassword;
                                    },
                                    child: !showPassword
                                        ? Text(
                                            'Show',
                                            style: GoogleFonts.roboto(
                                                color: Theme.of(context)
                                                    .backgroundColor),
                                          )
                                        : Text(
                                            'Hide',
                                            style: GoogleFonts.roboto(
                                                color: Theme.of(context)
                                                    .backgroundColor),
                                          )),
                                hintStyle: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 15),
                                enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey[300]!),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                border: UnderlineInputBorder(
                                  borderRadius: BorderRadius.circular(15),
                                )),
                            keyboardType: TextInputType.emailAddress,
                            validator: emailController.passwordValidator,
                            obscureText: !showPassword),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        height: 50,
                        child: TextFormField(
                            controller:
                                emailController.repeatPasswordController,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 15),
                            decoration: InputDecoration(
                                hintText: "Contraseña",
                                filled: true,
                                fillColor: Colors.grey[200],
                                suffixIcon: TextButton(
                                    onPressed: () {
                                      setState(() {});
                                      showPassword = !showPassword;
                                    },
                                    child: !showPassword
                                        ? Text(
                                            'Show',
                                            style: GoogleFonts.roboto(
                                                color: Theme.of(context)
                                                    .backgroundColor),
                                          )
                                        : Text(
                                            'Hide',
                                            style: GoogleFonts.roboto(
                                                color: Theme.of(context)
                                                    .backgroundColor),
                                          )),
                                hintStyle: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 15),
                                enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey[300]!),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                border: UnderlineInputBorder(
                                  borderRadius: BorderRadius.circular(15),
                                )),
                            keyboardType: TextInputType.emailAddress,
                            validator: emailController.passwordValidator,
                            obscureText: !showPassword),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        width: double.infinity,
                        // ignore: deprecated_member_use
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          onPressed: () {
                            if (_formKey.currentState?.validate() == true) {
                              emailController
                                  .createUserWithEmailAndPassword()
                                  .then(
                                      (value) => userController.createMyUser());
                            }
                          },
                          padding: const EdgeInsets.symmetric(vertical: 13),
                          color: Theme.of(context).primaryColor,
                          child: Text(
                            'Sign Up',
                            style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                                letterSpacing: 0.8),
                          ),
                        ),
                      ),
                      const Divider(),
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            '¿Ya tienes una cuenta?',
                            style: GoogleFonts.roboto(
                                color: Theme.of(context).backgroundColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                          ))
                    ],
                  ),
                ),
              ),
            )),
      ),
    );
  }
}
