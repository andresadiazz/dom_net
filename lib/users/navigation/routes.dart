import 'package:dom_n_net/hidden_drawer.dart';
import 'package:dom_n_net/users/ui/email_create_screen.dart';
import 'package:dom_n_net/users/ui/intro_screen.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get_navigation/src/routes/default_route.dart';

import '../../splash_screen.dart';

class Routes {
  static const splash = '/';
  static const intro = '/intro';
  static const createAccount = '/createAccount';
  static const singInEmail = '/signInEmail';
  static const home = '/home';

  static Route routes(RouteSettings settings) {
    switch (settings.name) {
      case splash:
        return _buildRoute(settings, page: const SplashScreen());
      case intro:
        return _buildRoute(settings, page: const IntroScreen());
      case createAccount:
        return _buildRoute(settings, page: const EmailCreate());
      case home:
        return _buildRoute(settings, page: const HiddenDrawer());

      default:
        throw Exception('Route does not exists');
    }
  }

  static GetPageRoute _buildRoute(RouteSettings settings,
          {required Widget page}) =>
      GetPageRoute(settings: settings, page: () => page);
}
