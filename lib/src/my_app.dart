import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../users/controllers/auth_controller.dart';
import '../users/navigation/routes.dart';

class MyApp extends StatelessWidget {
  final authController = Get.put(AuthController());
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
          appBarTheme: AppBarTheme(
              centerTitle: true,
              elevation: 0.0,
              titleTextStyle: GoogleFonts.roboto(
                  color: const Color(0xff69A1AC), fontSize: 20),
              iconTheme: const IconThemeData(color: Color(0xff69A1AC))),
          colorScheme: ColorScheme.fromSwatch()
              .copyWith(primary: const Color(0xffdde3e9)),
          primaryColor: const Color(0xff69A1AC),
          backgroundColor: const Color(0xff89C5CC),
          shadowColor: const Color(0xffF8FAFD)),
      title: 'Dom & Net App',
      onGenerateRoute: Routes.routes,
    );
  }
}
