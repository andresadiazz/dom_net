import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BienestarPage extends StatefulWidget {
  const BienestarPage({Key? key}) : super(key: key);

  @override
  State<BienestarPage> createState() => _BienestarPageState();
}

class _BienestarPageState extends State<BienestarPage> {
  @override
  Widget build(BuildContext context) {
    return _buildUI();
  }

  Widget _buildUI() {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.primary,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 18),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _dispotivosSection('Dispositivos de Entrada', Icons.mic,
                Icons.camera_alt, Icons.fingerprint),
            const SizedBox(
              height: 10,
            ),
            const Divider(),
            const SizedBox(
              height: 10,
            ),
            _dispotivosSection('Dispositivos de Salida', Icons.headphones,
                Icons.tv, Icons.clean_hands)
          ],
        ),
      ),
    );
  }

  Widget _dispotivosSection(
    String title,
    IconData icon1,
    IconData icon2,
    IconData icon3,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(
          title,
          style: GoogleFonts.roboto(fontSize: 20, fontWeight: FontWeight.w600),
        ),
        const SizedBox(
          height: 15,
        ),
        Wrap(
          children: [
            _cardSensor(icon1),
            _cardSensor(icon2),
            _cardSensor(icon3)
          ],
        )
      ],
    );
  }

  Widget _cardSensor(IconData iconSensor) {
    return SizedBox(
      height: 110,
      width: 124,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        shadowColor: const Color(0xff000000),
        elevation: 4,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              Align(
                  alignment: Alignment.topLeft,
                  child: Icon(
                    iconSensor,
                    size: 34,
                  )),
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  height: 8,
                  width: 8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.green,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
